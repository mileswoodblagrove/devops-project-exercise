terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">= 3.8"
    }
  }

  backend "azurerm" {
        resource_group_name  = "Cohort28_MilWoo_ProjectExercise"
        storage_account_name = "milwooprojexstoracc"
        container_name       = "milwooprojexcontainer"
        key                  = "terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
}

data "azurerm_resource_group" "main" {
  name     = "Cohort28_MilWoo_ProjectExercise"
}

resource "azurerm_service_plan" "main" {
  name                = "${var.prefix}-terraformed-asp"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_linux_web_app" "main" {
  name                = "${var.prefix}-milwoo-to-do-app-terraformed"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  service_plan_id     = azurerm_service_plan.main.id

  site_config {
    application_stack {
      docker_image_name = "mileswoodblagrove/todo-app:todo-app-prod"
    }
  }

  app_settings = {
    "DOCKER_REGISTRY_SERVER_URL" = "https://index.docker.io"
    "MONGO_CONNECTION_STRING" = azurerm_cosmosdb_account.main.primary_mongodb_connection_string
    "SECRET_KEY" = "secret-key"
    "MONGO_DB_NAME" = azurerm_cosmosdb_mongo_database.main.name
    "FLASK_APP" = "todo_app/app"
    "WEBSITES_PORT" = "8000"
    "LOG_LEVEL"="ERROR"
    "LOGGLY_TOKEN"="${var.loggly}"
  }
}

resource "azurerm_cosmosdb_account" "main" {
  name                = "${var.prefix}-milwoo-cosmos-db-account-terraformed"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  offer_type          = "Standard"
  kind                = "MongoDB"

  capabilities {
    name = "MongoDBv3.4"
  }

  capabilities {
    name = "EnableMongo"
  }

  capabilities {
    name = "EnableServerless"
  }

  consistency_policy {
    consistency_level       = "BoundedStaleness"
    max_interval_in_seconds = 300
    max_staleness_prefix    = 100000
  }

  geo_location {
    location          = "uksouth"
    failover_priority = 0
  }
}

resource "azurerm_cosmosdb_mongo_database" "main" {
  name                = "${var.prefix}-milwoo-cosmos-mongo-db-terraformed"
  resource_group_name = data.azurerm_resource_group.main.name
  account_name        = resource.azurerm_cosmosdb_account.main.name

  lifecycle { 
    prevent_destroy = true 
  }
}