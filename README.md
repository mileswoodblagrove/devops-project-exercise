# DevOps Apprenticeship: Project Exercise
> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements
The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.8+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)
```bash
curl -sSL https://install.python-poetry.org | python3 -
```

### Poetry installation (PowerShell)
```powershell
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```

## Dependencies
The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie. 

You may also wish to change the value of the LOG_LEVEL variable depending on how much logging you'd like when running locally. You will also need to create a [Loggly Token](https://www.loggly.com/) for the LOGGLY_TOKEN variable. After creating an account and logging in, select the “Logs” button on the left-hand side. From here open “Source Setup” and add a new "Customer Token". Copy the value of the token and set this in your .env file.

## MongoDB
### Set up MongoDB connection
In order to run the app locally, you will need to create an "Azure Cosmos DB for MongoDB account" in Azure. Store the PRIMARY CONNECTION STRING and DB name as variables in your .env file (this file is ignored by Git, your secrets are safe here).

## Running the App Locally
Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

## Running the App in Docker
To pull the prod image from [DockerHub](https://hub.docker.com/repository/docker/mileswoodblagrove/todo-app/general) directly, run:
```bash
docker pull mileswoodblagrove/todo-app:todo-app-prod
```

To build, run and publish the app to DockerHub in production mode run the commands:
```bash
docker build --target production --tag todo-app:prod .
docker run --env-file .env -p 7000:8000 todo-app:prod
docker push <user_name>/todo-app:todo-app-prod
```

To build in development mode which will allow the hot reload of code, run the build commands:
```bash
docker build --target development --tag todo-app:dev .
docker run --env-file .env -p 5000:5000 --mount type=bind,source="$(pwd)"/todo_app,target=/opt/todoapp/todo_app todo-app:dev
```
The app can also be launched in development mode by simply running docker compose as follows:
```bash
docker compose up
```

## Testing
Once the all dependencies have been installed, run the test suite by running:
```bash
$ poetry run pytest
```

This will run the tests associated with this application.

## Running the App from Azure
The Azure hosted app can be found at https://milwoo-to-do-app.azurewebsites.net/.

## Encryption Details
Both "encryption-at-rest" and "encryption-in-transit" are implemented in Azure Cosmos DB, so that our stored data is encrypted on disk, and all connections to the database are encrypted. In addition, the app is only accessible over HTTPS. For more information on encryption see [here](https://learn.microsoft.com/en-us/azure/cosmos-db/database-encryption-at-rest).

## Terraform
To apply Azure resource changes via Terraform after installing, from a terminal run the following commands to initialise your directory locally, and then login to the Azure CLI:
```bash
terraform init
```
```bash
az login
```

Once you are happy with your defined resources in the main.tf file you can run terraform plan to see what changes will be applied if you then run terraform apply.
```bash
terraform plan
```
```bash
terraform apply
```