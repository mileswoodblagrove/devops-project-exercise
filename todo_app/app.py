from glob import escape
import os
from flask import Flask, redirect, render_template, request
from todo_app.data.mongo_items import create_new_mongo_item, get_items_from_mongo, update_mongo_item
from todo_app.item import Item
from todo_app.view_model import ViewModel
from todo_app.flask_config import Config
from loggly.handlers import HTTPSHandler
from logging import Formatter

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config())
    app.logger.setLevel(app.config['LOG_LEVEL'])
    
    if app.config['LOGGLY_TOKEN'] is not None:
        handler = HTTPSHandler(f'https://logs-01.loggly.com/inputs/{app.config["LOGGLY_TOKEN"]}/tag/todo-app')
        handler.setFormatter(
            Formatter("[%(asctime)s] %(levelname)s in %(module)s: %(message)s")
        )
        app.logger.addHandler(handler)

    @app.route('/')
    def index():
        try:
            documents = get_items_from_mongo()
            items = []
            for document in documents:
                item = Item.from_mongo_item(document)
                items.append(item)
            item_view_model = ViewModel(items)
            app.logger.debug(f"Successfully loaded items from Mongo")  
        except Exception as e:
            app.logger.error(f"An error occurred whilst loading items from Mongo. {e}")
        return render_template("index.html", view_model = item_view_model)

    @app.route('/add-new-item', methods=["POST"])
    def add_new_task():
        try:
            task_name = request.form.get("task_name")
            create_new_mongo_item(task_name)
            app.logger.debug(f"Successfully added new item: {task_name}")
        except Exception as e:
            app.logger.error(f"An error occurred whilst adding new item. {e}")
        return redirect("/")

    @app.route('/update-card-status/<card_id>', methods=["POST"])
    def update_mongo_item_status(card_id):
        try:
            update_mongo_item(card_id)
            app.logger.debug(f"Successfully updated status of item: {card_id}")
        except Exception as e:
            app.logger.error(f"An error occurred whilst updating status of item: {card_id}. {e}")
        return redirect("/")
    
    app.logger.info("Successfully initialised application.")

    return app