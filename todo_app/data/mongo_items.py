import os
import pymongo

def get_mongo_connection_string():
    return os.getenv('MONGO_CONNECTION_STRING')

def get_mongo_db_name():
    return os.getenv('MONGO_DB_NAME')

def get_mongo_collection():
    conn_str = os.getenv('MONGO_CONNECTION_STRING')
    client = pymongo.MongoClient(conn_str)
    db_name = os.getenv('MONGO_DB_NAME')
    db = client[db_name]
    collection = db['items']
    return collection

def get_items_from_mongo():
    collection = get_mongo_collection()
    items = collection.find()
    return items

def create_new_mongo_item(task_name):
    new_item = { 'status': 'Not Started', 'name': f'{task_name}' }
    collection = get_mongo_collection()
    collection.insert_one(new_item)

def update_mongo_item(id):
    query = {"_id": f"{id}"}
    new_status = { "$set": { "status": "Done" } }
    collection = get_mongo_collection()
    collection.update_one(query, new_status)
