class ViewModel:
    def __init__(self, items):
        self._items = items
 
    @property
    def items(self):
        return self._items

    @property
    def done_items(self):
        done_items = self.get_filtered_items_from_status(self.items, "Done")
        return done_items

    @property
    def doing_items(self):
        doing_items = self.get_filtered_items_from_status(self.items, "Doing")
        return doing_items
    
    @property
    def to_do_items(self):
        to_do_items = self.get_filtered_items_from_status(self.items, "To Do")
        return to_do_items  
    
    def get_filtered_items_from_status(self, items_list, status):
        filtered_items = []

        for item in items_list:
            if item.status == status:
                filtered_items.append(item)

        return filtered_items