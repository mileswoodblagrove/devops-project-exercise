class Item:
    def __init__(self, id, name, status = 'To Do'):
        self.id = id
        self.name = name
        self.status = status

    @classmethod
    def from_mongo_item(cls, item):
        return cls(item['_id'], item['name'], item['status'])