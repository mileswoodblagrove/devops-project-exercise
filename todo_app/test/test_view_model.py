import pytest
from todo_app.item import Item
from todo_app.view_model import ViewModel

class TestViewModel:

    # Arrange
    @pytest.fixture()
    @staticmethod
    def items():
        item_one = Item("123", "Test_Done", "Done")
        item_two = Item("456", "Test_Doing", "Doing")
        item_three = Item("789", "Test_To_Do", "To Do")
        return [item_one, item_two, item_three]


    @staticmethod
    def test_view_model_returns_done_items(items):
        # Act
        view_model = ViewModel(items)
        done_items = view_model.done_items

        # Assert
        assert len(done_items) == 1

    @staticmethod
    def test_view_model_returns_doing_items(items):
        view_model = ViewModel(items)
        doing_items = view_model.doing_items

        assert len(doing_items) == 1

    @staticmethod
    def test_view_model_returns_to_do_items(items):
        view_model = ViewModel(items)
        to_do_items = view_model.to_do_items

        assert len(to_do_items) == 1             