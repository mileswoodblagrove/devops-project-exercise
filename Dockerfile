FROM python:3.12 as base
RUN curl -sSL https://install.python-poetry.org | python3 -
WORKDIR /opt/todoapp
COPY pyproject.toml poetry.lock ./
RUN /root/.local/bin/poetry install
COPY ./todo_app ./todo_app

FROM base as production

ENTRYPOINT ["/root/.local/bin/poetry", "run", "gunicorn", "--bind", "0.0.0.0", "todo_app.app:create_app()"]

FROM base as development

ENTRYPOINT ["/root/.local/bin/poetry", "run", "flask", "run", "--host", "0.0.0.0"]

FROM base as test

ENTRYPOINT ["/root/.local/bin/poetry", "run", "pytest"]